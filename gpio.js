const getStatus = (callback) => {
    let led1, led2, r

    r = Math.random()   // 0 ~ 1
    led1 = r > 0.4 ? 'on' : 'off'
    r = Math.random()
    led2 = r > 0.8 ? 'on' : 'off'
    
    let status = {
        led1: led1,
        led2: led2
    }

    callback(status)
}
const led = (id, command) => {
    console.log(id, command)

    if (!command) command = id

    if (command == 'on') {
        console.log(`led id:${id} is now on`)
    } else {
        console.log(`led id:${id} is now off`)
    }
}

const button = (id, command) => {
    console.log(id, command)

    if (!command) command = id

    if (command == 'on') {
        console.log(`button id:${id} is now on`)
    } else {
        console.log(`button id:${id} is now off`)
    }
}

module.exports = {
    getStatus,
    led,
    button
}