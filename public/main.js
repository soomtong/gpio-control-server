const socket = io()

showIP(ip => updateMyIP(ip))
bindSend()
bindReceive()
bindButton()

function bindSend() {
	$(function() {
		$('#send').bind('click', function() {
			let message = $('#message').val()

			if (message) sendMessage(message)
		})
	})
}

function bindReceive() {
	$(function() {
        socket.on('event', updateLEDState)
		socket.on('chat', function(msg) {
			$('#message_list').append($('<li>').text(msg))
		})

        socket.emit('chat', 'Hi, every one!')
	})
}

function bindButton () {
    $(function () {
        $('#button1').bind('click', function () {
            const led1Command = $('#led1').text() == 'on' ? 'off' : 'on'

            socket.emit('event', { id: 1, command: led1Command })
        })

        $('#button2').bind('click', function () {
            const led1Command = $('#led2').text() == 'on' ? 'off' : 'on'

            socket.emit('event', { id: 2, command: led1Command })
        })
    })
}

function updateLEDState(data) {
    switch (data.id) {
        case 1:
            $('#led1').text(data.command)
            break
        case 2:
            $('#led2').text(data.command)
            break
        default:
            break
    }

    console.log(data)
}

function sendMessage(msg) {
	console.log(msg)
	socket.emit('chat', msg)
}

function updateMyIP(ip) {
	$('#client_ip').text(ip)
}

function showIP(callback) {
	const serviceHost = 'http://httpbin.org'
	const command = '/ip'

	$.get(serviceHost + command, (data, result, xhr) => {
		if (data && data.origin) callback(data.origin)
		else console.error('ajax request is failed')
	})
}
