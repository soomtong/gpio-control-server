const path = require('path')   
const http = require('http')
const socketIO = require('socket.io')
const express = require('express')
const twig = require('twig')
const gpio = require('./gpio')

const app = express()
const server = http.Server(app)
const io = socketIO(server)

const port = 3000

twig.cache(false)
app.engine('html', twig.__express)
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'html')

app.use(express.static(path.join(__dirname, 'public')));

io.on('connect', function (socket) {
    console.log('a browser connected')

    socket.on('disconnect', function () {
        console.log('browser disconnected')
    })

    socket.on('event', function (data) {
        // gpio.button(data.id, data.command)
        gpio.led(data.id, data.command)

        io.emit('event', data)
    })

    socket.on('chat', function (msg) {
        io.emit('chat', msg)
    })
})

app.get('/', (req, res) => {
    gpio.getStatus((status) => {
        console.log(status)

        io.emit('event', { status })

        res.render('index.html', { msg: '안녕하세요!',
                                   status: status })
    })
})

// button router
app.get('/button/:command/:id?', (req, res) => {
    if (!req.params.command) { return res.sendStatus(404) }

    const command = req.params.command == 'on' ? 'on' : 'off'

    if (req.xhr) {
        if (req.params.id) {
            const id = req.params.id
    
            gpio.button(id, command)
            res.send({ result: 'ok', 
                    id: id, command:command })
        } else {
            gpio.button(command)
            res.send({ result: 'ok', 
                    command: command })
        }    
    } else {
        if (req.params.id) {
            const id = req.params.id
    
            gpio.button(id, command)
            res.render('button.html', 
                { id: id, command: command })
        } else {
            gpio.button(command)
            res.render('button.html', 
                { command: command })
        }    
    }
})

// led router
app.get('/led/:command/:id?', (req, res) => {
    if (!req.params.command) { return res.sendStatus(404) }

    const command = req.params.command == 'on' ? 'on' : 'off'

    if (req.xhr) {
        if (req.params.id) {
            const id = req.params.id
    
            gpio.led(id, command)
            res.send({ result: 'ok', 
                    id: id, command:command })
        } else {
            gpio.led(command)
            res.send({ result: 'ok', 
                    command: command })
        }    
    } else {
        if (req.params.id) {
            const id = req.params.id
    
            gpio.led(id, command)
            res.render('led.html', 
                { id: id, command: command })
        } else {
            gpio.led(command)
            res.render('led.html', 
                { command: command })
        }    
    }
})


// start web server
server.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
})
